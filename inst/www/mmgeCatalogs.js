(function() {

  // Object for keeping track of row selections.\
  function RowSelections() {
    var that = this;
    this.ids = [];
    this.add = function(id) {
      if(!(that.ids instanceof Array)) {
        that.ids = [that.ids];
      }
      if(that.ids.indexOf(id) === -1) {
        that.ids.push(id);
      }
      return that;
    };
    this.remove = function(id) {
      if(!(that.ids instanceof Array)) {
        that.ids = [that.ids];
      }
      var i = that.ids.indexOf(id);
      if(i > -1) {
        that.ids.splice(i, 1);
      }
      return that;
    };
    this.isSelected = function(id) {
      return that.ids.indexOf(id) > -1;
    };
    this.set = function(ids) {
      if(!(ids instanceof Array)) {
        ids = [ids];
      }
      that.ids = ids;
    };
    this.get = function() {
      if(!(that.ids instanceof Array)) {
        that.ids = [that.ids];
      }
      return that.ids;
    };
    this.updateServer = function() {
      Shiny.onInputChange("selected_rows", that.ids);
    };
    this.getId = function(row) {
      mmgeCatalogs.catalogTable.row(row).data()[0];
    };
    this.updateSelections = function() {
      mmgeCatalogs.catalogTable.rows().every(function(i, t, r) {
        $(this.node()).toggleClass("selected", that.isSelected(this.data()[0]));
      });
    };

    this.length = this.ids.length;

  }

  var exports = window.mmgeCatalogs = window.mmgeCatalogs || {
    table_id: "#catalogTable",
    catalogTable: null,
    selected_rows: new RowSelection(),
    last_row_clicked: null,
  };

  exports.setDataTableObject = function(scope) {
    scope = (typeof scope === 'undefined') ? $(mmgeCatalogs.table_id) : scope;
    mmgeCatalogs.catalogTable = $(scope).find("table").DataTable();
  };

  exports.initializeCatalogTable = function() {

    $("#columnControlFilterSearch").on("keyup", function(e) {

      var re = new RegExp($("#columnControlFilterSearch").val(), "i");

      $(".catalog-control").each(function(i) {
        if($(this).data("column").match(re) === null) {
          $(this).css("display", "none");
        } else {
          $(this).css("display", "block");
        }
      });

    });

    $("#columnControlFilterReset").on("click", function(e) {
      $("#columnControlFilterSearch").val("").trigger("keyup");
    });

    $(mmgeCatalogs.table_id).off("draw.dt.mmge").on("draw.dt.mmge", function() {
      mmgeCatalogs.setDataTableObject();
      updateFooter();
      updateRowSelection();
      setTableHeight();
    });
    $(mmgeCatalogs.table_id).off("click.mmge").on("click.mmge", "tr", function(e, f) {
      if($(this).hasClass("selected")) {
        addRowSelection(this, e);
      } else {
        removeRowSelection(this, e);
      }
      updateServerRowSelection();
      $("#download_selected").parent().toggleClass("disabled", mmgeCatalogs.selected_rows.length === 0);
      updateFooter();
    });
  };

  // Function called after an upload to prevent further uploads
  exports.disableUpload = function(message) {
    $("#join_button").addClass("disabled");
    $("#joinModal").modal("hide");
    var alert_dismiss = setTimeout(function() {
      $("#joinAlert").alert("close");
    }, 10000);
    $("#joinAlert").on("mouseover", function() {
      clearTimeout(alert_dismiss);
    });
    $("#joinAlert").on("mouseout", function() {
      alert_dismiss = setTimeout(function() {
        $("#joinAlert").alert("close");
      }, 5000);
    });
  };

  // Toggle column visibility
  exports.columnToggle = function(column, visible) {
    $(mmgeCatalogs.table_id + ' table').DataTable()
      .column(column + ":name").visible(visible);
    setTableHeight();
  };

  // Apply search filters to a column
  exports.columnSearch = function(column, type, filter, filter_missing) {

    var data = JSON.stringify({
      "filter": filter,
      "type": type,
      "filter_missing": filter_missing
    });

    $(mmgeCatalogs.table_id + ' table')
      .DataTable()
      .column(column + ":name")
      .search(data)
      .draw();
  };

  // Reset a column filter to its default value
  exports.resetFilter = function($el) {
    var s,
        $filter =  $el.find("#" + $el.attr("id") + "_filter");
    if($el.data("filter_type") == "slider") {
      s = $filter.data("ionRangeSlider");
      s.update({
        from: s.options.min,
        to: s.options.max
      });
    } else if($el.data("filter_type") == "select") {
       s = $filter.data("selectize");
       s.clear();
    } else {
      s = $filter.data("shinyInputBinding");
      s.setValue($filter[0], "");
      $filter.trigger("change");
    }
  };

  function addRowSelection(row, event) {
    var current_row;
    if(event.shiftKey & mmgeCatalogs.last_row_clicked !== null) {
      var last_index = mmgeCatalogs.catalogTable.row(mmgeCatalogs.last_row_clicked).index(),
          this_index = mmgeCatalogs.catalogTable.row(row).index();
      for(var i = Math.min(last_index, this_index); i <= Math.max(last_index, this_index); i++) {
        current_row = mmgeCatalogs.catalogTable.row(i);
        $(current_row.node()).addClass("selected");
        mmgeCatalogs.selected_rows.add(current_row.data()[0]);
      }
    } else {
      mmgeCatalogs.selected_rows.add(mmgeCatalogs.catalogTable.row(row).data()[0]);
    }

    mmgeCatalogs.last_row_clicked = row;

  }

  function removeRowSelection(row, event) {
    mmgeCatalogs.selected_rows.remove(mmgeCatalogs.catalogTable.row(row).data()[0]);
    mmgeCatalogs.last_row_clicked = null;
  }

  function updateRowSelection() {

  }

  updateServerRowSelection = function() {
    Shiny.onInputChange("selected_rows", mmgeCatalogs.selected_rows.get());
  };

  function updateFooter() {
      var table = $(mmgeCatalogs.table_id + ' table').DataTable();
      var pi = table.page.info();
      $(".table-footer").text("Displaying " + pi.recordsDisplay + " records filtered from " + pi.recordsTotal + " total (" + mmgeCatalogs.selected_rows.length + " Selected)");
  }

  function setTableHeight() {
    var h = window.innerHeight - ($(".main-header").height() +
            $(".table-footer").height() +
            $(".dataTables_scrollHead").height());
    $(mmgeCatalogs.table_id + " .dataTables_scrollBody").css("height", h + "px");
  }

  function initializeHelpMenu() {
    $("#helpModal .help-tab").on("click", function(e) {
      $("#helpModal .help-page").removeClass("shown");
      $($(this).data("target")).addClass("shown");
      $("#helpModal .help-tab").parent().removeClass("active");
      $(this).parent().addClass("active");
    });
  }

  exports.columnControlBinding = new Shiny.InputBinding();
  $.extend(exports.columnControlBinding, {
    find: function(scope) {
      return $(scope).find(".catalog-control");
    },
    getValue: function(el) {
      var $el = $(el);
      var id = $el.attr("id");
      var value = {};
      value.filter = Shiny.shinyapp.$inputValues[id + "_filter"];
      value.remove_missing = Shiny.shinyapp.$inputValues[id + "_missing"];
      value.filter_type = $el.data("filter_type");
      value.filter_visible = $el.find(".collapse").hasClass("in");
      value.column_visible = Shiny.shinyapp.$inputValues[id + "_show"];
      value.column_name = $el.data("column");
      return value;
    },
    setValue: function(el, value) {
      var $el = $(el);
      var id = $el.attr("id");
    },
    subscribe: function(el, callback) {
      $(el).on("catalogChange", function(e, binding, bindingType) {
        callback();
      });
      $(el).find(".collapse").on("shown.bs.collapse, hidden.bs.collapse", function(e) {
        callback();
      });
    },
    initialize: function(el) {

      var $el = $(el);
      var id = $el.attr("id");

      var $show = $el.find("#" + id + "_show");
      var $filter = $el.find("#" + id + "_filter");
      var $missing = $el.find("#" + id + "_missing");

      $el.on("catalogChange.show", function(e) {
        mmgeCatalogs.setTableHeight();
      });

      var column = $el.data("column");

      $show.on("change", function(e) {
        var checked = $show.is(":checked");
        mmgeCatalogs.columnToggle(column, checked);
        $el.trigger("catalogChange.show", checked);
      });

      $el.find(".dismiss-column").on("click", function(e) {
        try {$show.prop("checked", false);} catch(err) {}
        try {mmgeCatalogs.resetFilter($el);} catch(err) {}
        try {mmgeCatalogs.columnToggle(column, false);} catch(err) {}
        try {$el.css("display", "none");} catch(err) {}
      });

      $el.find(".filter-reset").on("click", function(e) {
        mmgeCatalogs.resetFilter($el);
      });

      $missing.on("change", function(e) {
        var checked = $missing.is(":checked");
        mmgeCatalogs.columnSearch(
          column,
          $el.data("filter_type"),
          Shiny.shinyapp.$inputValues[id + "_filter"],
          checked
        );
        $el.trigger("catalogChange.missing", checked);
      });

      function bindFilter(binding, el) {

        binding.subscribe(el, function() {
          mmgeCatalogs.columnSearch(
            column,
            $el.data("filter_type"),
            binding.getValue(el),
            $missing.is(":checked"));
          $el.trigger("catalogChange.filter", binding.getValue(el));
        });
      }

      if($filter.data("shinyInputBinding") === undefined) {
        $filter.on("shiny:bound", function(binding, bindingType) {
          bindFilter(binding.binding, this);
        });
      } else {
        bindFilter($filter.data("shinyInputBinding"), $filter[0]);
      }

    }
  });
  Shiny.inputBindings.register(exports.columnControlBinding);

  Shiny.addCustomMessageHandler("disableFileUpload", mmgeCatalogs.disableUpload);
  Shiny.addCustomMessageHandler("updateRowSelection", function(message) {
    if(message === null) {
      message = [];
    }
    mmgeCatalogs.selected_rows.set(message);
    updateServerRowSelection();
    updateRowSelection();
    updateFooter();
  });
  Shiny.addCustomMessageHandler("column_toggle", function(message) {
    var columns = Object.keys(message);
    for(var i = 0; i < columns.length; i++) {
      mmgeCatalogs.columnToggle(columns[i], message[columns[i]]);
    }
  });

  $(document).ready(function() {

    $(mmgeCatalogs.table_id).on("init.dt", function(e) {
      mmgeCatalogs.setDataTableObject(this);
    });

    initializeHelpMenu();

  });

  $(window).resize(mmgeCatalogs.setTableHeight);

})();