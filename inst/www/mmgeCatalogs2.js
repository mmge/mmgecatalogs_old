(function() {

  function catalogControl(id, catalog) {

    var control = this;
    this.id = id;
    this.catalog = catalog;
    this.control = $("#" + this.id);
    this.columnName = this.control.data("column");
    this.filterType = this.control.data("filter_type");
    this.inputBinding = this.control.data("shinyInputBinding");

    this.visibilityControl = this.control.find("#" + id + "_show");
    this.filterControl = this.control.find("#" + id + "_filter");
    this.missingControl = this.control.find("#" + id + "_missing");
    this.filterToggle = this.control.find(".filter-toggle");
    this.filterReset = this.control.find(".filter-reset");
    this.columnDismiss = this.control.find(".dismiss-column");
    this.filter = Shiny.shinyapp.$inputValues[control.id];

    this.column = function() {
      try {
        return control.catalog.catalogTable.column(this.columnName + ":name");
      } catch(err) {
        return false;
      }
    };

    this.trigger = function(event) {
      $(control).trigger(event);
    };

    this.on = function(event, handler) {
      $(control).on(event, handler);
    };

    this.search = function() {
      if(control.filterControl.data("shinyInputBinding")) {
        var data = JSON.stringify({
          "filter": control.filterControl.data("shinyInputBinding").getValue(control.filterControl[0]),
          "type": control.filterType,
          "filter_missing": control.missingControl.is(":checked")
        }),
        column = control.column();
        if(column) {
          control.column().search(data).draw();
          control.control.trigger("catalogChange");
        }
      }
    };

    control.control.on("catalogChange", function() {
      if(control.isDefault()) {
        control.filterReset.addClass("hidden");
      } else {
        control.filterReset.removeClass("hidden");
      }
    });

    this.resetFilter = function() {
      var s;
      if(control.filterType == "slider") {
        s = control.filterControl.data("ionRangeSlider");
        s.update({
          from: s.options.min,
          to: s.options.max
        });
      } else if(control.filterType == "select") {
        control.filterControl.data("selectize").clear();
      } else {
        control.filterControl.data("shinyInputBinding").setValue(control.filterControl[0], "");
      }
      control.missingControl.prop("checked", false);
      control.filterControl.trigger("change");
    };

    this.isDefault = function() {
      if(control.filterType == "slider") {
        s = control.filterControl.data("ionRangeSlider");
        return s.result.from == s.options.min && s.result.to == s.options.max && !control.missingControl.is(":checked");
      } else if(control.filterType == "select") {
        s = control.filterControl.data("selectize");
        return s.items.length === 0 && !control.missingControl.is(":checked");
      } else {
        return control.filterControl.data("shinyInputBinding").getValue(control.filterControl[0]) === "" && !control.missingControl.is(":checked");
      }
    };

    this.toggleColumnVisibility = function(visible) {
      visible = (typeof visible === 'undefined') ? control.visibilityControl.is(":checked") : visible;
      if(control.column()) {
        control.column().visible(visible);
        control.catalog.trigger("heightCheck");
        control.control.trigger("catalogChange");
      }
    };

    function bindFilter(binding, el) {

      binding.subscribe(el, function() {
        control.filter = binding.getValue(el);
        control.trigger("search");
      });

    }

    if(this.filterControl.data("shinyInputBinding") === undefined) {
      this.filterControl.on("shiny:bound", function(binding, bindingType) {
        bindFilter(binding.binding, this);
      });
    } else {
      bindFilter(this.filterControl.data("shinyInputBinding"), this.control[0]);
    }

    this.visibilityControl.on("change", function() {
      control.toggleColumnVisibility();
    });
    this.filterReset.on("click", this.resetFilter);
    this.missingControl.on("change", function() {control.trigger("search")});
    this.on("search", this.search());

  }

  function catalogSelections(catalog) {

    var that = this;
    this.catalog = catalog;
        this.ids = [];
    this.add = function(row) {
      var id = that.getId(row);
      if(!(that.ids instanceof Array)) {
        that.ids = [that.ids];
      }
      if(that.ids.indexOf(id) === -1) {
        that.ids.push(id);
      }
      $(that.catalog.catalogTable.row(row).node()).addClass("selected");
      that.length = that.ids.length;
      that.updateServer();
      $(that).trigger("selection");
      return that;
    };
    this.remove = function(row) {
      var id = that.getId(row);
      if(!(that.ids instanceof Array)) {
        that.ids = [that.ids];
      }
      var i = that.ids.indexOf(id);
      if(i > -1) {
        that.ids.splice(i, 1);
      }
      $(that.catalog.catalogTable.row(row).node()).removeClass("selected");
      that.length = that.ids.length;
      that.updateServer();
      $(that).trigger("selection");
      return that;
    };
    this.isSelected = function(row) {
      var id = that.getId(row);
      return that.ids.indexOf(id) > -1;
    };
    this.set = function(ids) {
      if(!(ids instanceof Array)) {
        ids = [ids];
      }
      that.ids = ids;
      that.length = that.ids.length;
      that.updateSelections();
      that.updateServer();
      $(that).trigger("selection");
      return that;
    };
    this.get = function() {
      if(!(that.ids instanceof Array)) {
        that.ids = [that.ids];
      }
      return that.ids;
    };
    this.updateServer = function() {
      Shiny.onInputChange("selected_rows", that.ids);
    };
    this.getId = function(row) {
      return that.catalog.catalogTable.row(row).data()[0];
    };
    this.updateSelections = function() {
      that.catalog.catalogTable.rows().every(function(i, t, r) {
        $(this.node()).toggleClass("selected", that.isSelected(this));
      });
    };

    this.length = this.ids.length;

  }

  function mmgeCatalogs(table_id) {

    var catalog = this;
    var $catalog = $(this);
    var $this = $(this);

    this.selected = new catalogSelections(this);
    this.table_id = table_id;
    this.controls = {};
    this.catalogTable = null;
    this.last_row_clicked = null;

    this.on = function(events, selector, data, handler) {
      $catalog.on.apply($catalog, arguments);
      return catalog;
    };

    this.trigger = function(event, extraParameters) {
      $catalog.trigger.apply($catalog, arguments);
      return catalog;
    };

    this.setCatalogTable = function(scope) {
      scope = (typeof scope === 'undefined') ? $(catalog.table_id) : scope;
      catalog.catalogTable = $(scope).find("table").DataTable();
      return catalog;
    };

    $(this.table_id)
      .off("draw.dt.mmgeCatalogs")
      .on("draw.dt.mmgeCatalogs", function() {
        catalog.setCatalogTable()
          .trigger("heightCheck")
          .trigger("footerUpdate");
        catalog.selected.updateSelections();
      });

    $(this.table_id).off("click.mmgeCatalogs")
      .on("click.mmgeCatalogs", "tr", function(e) {
        if($(this).hasClass("selected")) {
          var current_row;
          if(e.shiftKey & catalog.last_row_clicked !== null) {
            var last_index = catalog.catalogTable.row(catalog.last_row_clicked).index(),
                this_index = catalog.catalogTable.row(this).index();
            for(var i = Math.min(last_index, this_index); i <= Math.max(last_index, this_index); i++) {
              current_row = catalog.catalogTable.row(i);
              catalog.selected.add(current_row);
            }
          } else {
            catalog.selected.add(this);
          }
          catalog.last_row_clicked = this;
        } else {
          catalog.selected.remove(this);
          catalog.last_row_clicked = null;
        }
        $("#download_selected").parent().toggleClass("disabled", catalog.selected.length === 0);
        catalog.trigger("footerUpdate");
      });

    $("#columnControlFilterSearch").on("keyup", function() {

      var re = new RegExp($("#columnControlFilterSearch").val(), "i");

      $(".catalog-control").each(function(i) {
        $(this).toggleClass("hidden", $(this).data("column").match(re) === null);
      });

    });

    $("#columnControlFilterReset").on("click", function(e) {
      $("#columnControlFilterSearch").val("").trigger("keyup");
    });

    this.on("footerUpdate", function() {
      function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
      }
      var pi = catalog.catalogTable.page.info(),
          msg = "Displaying " + numberWithCommas(pi.recordsDisplay) + " records";
      if(pi.recordsDisplay !== pi.recordsTotal) {
        msg = msg + " filtered from " + numberWithCommas(pi.recordsTotal) + " total";
      }
      msg = msg + " (" + numberWithCommas(catalog.selected.length) + " Selected)";
      $(".table-footer").text(msg);
    });

    this.on("heightCheck", function() {
      var h = window.innerHeight - ($(".main-header").height() +
              $(".table-footer").height() +
              $(".dataTables_scrollHead").height());
      var h2 = window.innerHeight - ($(".main-header").height() +
              $("#columnControlFilter").height() + 20);
      $(catalog.table_id + " .dataTables_scrollBody").css("height", h + "px");
      $("#menu").css("height", h2 + "px");

    });

    columnControlBinding = new Shiny.InputBinding();
    $.extend(columnControlBinding, {
      find: function(scope) {
        return $(scope).find(".catalog-control");
      },
      getValue: function(el) {
        var $el = $(el);
        var id = $el.attr("id");
        var value = {};
        value.filter = Shiny.shinyapp.$inputValues[id + "_filter"];
        value.remove_missing = Shiny.shinyapp.$inputValues[id + "_missing"];
        value.filter_type = $el.data("filter_type");
        value.filter_visible = $el.find(".collapse").hasClass("in");
        value.column_visible = Shiny.shinyapp.$inputValues[id + "_show"];
        value.column_name = $el.data("column");
        return value;
      },
      setValue: function(el, value) {
        var $el = $(el);
        var id = $el.attr("id");
      },
      subscribe: function(el, callback) {
        $(el).on("catalogChange", function(e, binding, bindingType) {
          callback();
        });
        $(el).find(".collapse").on("shown.bs.collapse, hidden.bs.collapse", function(e) {
          callback();
        });
      },
      initialize: function(el) {

        var $el = $(el);
        var control = new catalogControl($el.attr("id"), catalog);
        catalog.controls[$el.data('column')] = control;

      }
    });
    Shiny.inputBindings.register(columnControlBinding);

    Shiny.addCustomMessageHandler("updateRowSelection", function(message) {
      if(message === null) {
        message = [];
      }
      catalog.selected.set(message);
      catalog.trigger("footerUpdate");
    });

    $(window).resize(function() {catalog.trigger("heightCheck")});
//    catalog.simplebar = new SimpleBar(document.getElementById("menu_container"));
  }



  window.mmgeCatalogs = mmgeCatalogs;

})();

function initializeCatalog() {
//  new SimpleBar(document.getElementById("menu_container"));
  window.catalog = new window.mmgeCatalogs("#catalogTable");
  window.catalog.trigger("heightCheck");
  $("#helpModal .help-tab").on("click", function(e) {
    $("#helpModal .help-page").removeClass("shown");
    $($(this).data("target")).addClass("shown");
    $("#helpModal .help-tab").parent().removeClass("active");
    $(this).parent().addClass("active");
  });
}