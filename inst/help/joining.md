# Joining External Data

This catalog supports joining external data so that researchers can easily filter
and select specimens based on criteria outside of those available in the base
catalog. To access this functionality click the "Join Data" button in the toolbar.

![Join Data Button](mmgeCatalogs/images/join_button.png)

Clicking this button opens the Join Data modal window:

![Join Data Modal](mmgeCatalogs/images/join_modal.png)

Click the "Browse" button to open a file dialog that allows you to select files
to join to the base catalog. You may join multiple files to the catalog at once.

**PLEASE NOTE:** You must select all files that you want to join to the dataset at
one time. Once you have joined data, the "Join Data" button is disabled for your current session and you will not be able to join additional files. 

After you have selected files to join close the Join Data Modal. An alert box will
appear at the top of the screen letting you know that status of your data joins.

## Join Status

If all joins were successful, the alert will be green. Please examine the alert
and ensure that all joined files were joined on the expected fields.

![Join Data Success](mmgeCatalogs/images/join_success.png)

If some files successfully joined, but other did not, the alert will be orange and 
will tell you which files were joined and which were not.

![Join Data Some Success](mmgeCatalogs/images/join_some_success.png)

If none of the files you had selected could be joined the alert will be red.

![Join Data Failure](mmgeCatalogs/images/join_failed.png)

## Joinable data

The PREDICT-HD Catalog currently supports two types of files for joining. 

### dbGaP Files

If the uploaded file has a `txt` extension the catalog assumes that it is a file from
dbGaP and will treat it as such. It will join based on `SUBJID` and `EVENT` if those 
fields are present in the joined file. It is important that you don't modify the 
dbGaP file before uploading it. The catalog expects that the files will be formatted
in a very specific way. If you change the formatting it could result in unexpected
results. If you need to upload a customized file, see the csv option below.

### CSV Files

If you wish to join data other than those files from dbGaP they must be in CSV format.
If the uploaded file has a `csv` extension the catalog will attempt to join the file based on column headers titled **EXACTLY** `SUBJID` and `EVENT`. It is very important that if you are creating custom files to join that your columns have those exact names or the catalog will not recognize them. You must also ensure that the first row of your csv file contains the column headers. If your file has additional meta data above the headers or blank rows it may cause the join to fail.

