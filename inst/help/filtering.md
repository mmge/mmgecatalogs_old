# Filtering Catalog Records

Use the sidebar at the left to filter catalog records. Each field in the catalog 
is represented by a control in the sidebar.

![Catalog Control](mmgeCatalogs/images/filter.png)

* Clicking the checkbox to the left of the field name will toggle the visibility
  of that column in the catalog. 
* The <i class = "fa fa-filter"></i> button to the right of the field name will 
  display the filtering tool for that field. 
* If you have filtered a column a <i class = "fa fa-undo"></i> button will appear
  next to the <i class = "fa fa-filter"></i> button. Clicking this button will 
  reset all filtering for this field.
  
## Filter Types

There are three main filter types: text search, select box, and slider. Based on
the type of data in the field one of these three filters will be available to you.

### Text Search Filter

![Text Search Filter](mmgeCatalogs/images/search_filter.png)

The most basic type of filter is the text search filter. If the field contains
many different text values this type of filter will be available. It provides a simple 
text input. Any value you type into the text box will be searched for in the 
field. Only records that match the search query will remain. For advanced users
this filter does accept regular expressions.

### Select Box Filter

![Select Box Filter](mmgeCatalogs/images/select_filter.png)

This type of filter is available when only a few unique values appear within a 
field. It accepts multiple selections so you can filter based on as many or as 
few values as you like.

### Slider Filter

![Slider Filter](mmgeCatalogs/images/slider_filter.png)

This type of filter is available when there are many different numeric values in
a field. It allows you to select a range of values. Click and drag the large circles
on either side of the bar to filter the field. Only those values that fall within the 
blue section of the bar will remain in the catalog.

## Remove Missing Values

Each filter also contains a "Remove Missing Values" checkbox. The filters mentioned
above only act on records that contain data in the given field. Blank values will 
remain unless you check the "Remove Missing Values" checkbox.

## Reset Filters

If you have filtered the catalog based on a given field a <i class = "fa fa-undo"></i>
button will appear on the control. Clicking the <i class = "fa fa-undo"></i> button will remove any filters based on that field.

![Filtered Filter](mmgeCatalogs/images/slider_filtered.png)
  
  