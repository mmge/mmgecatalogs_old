write_app_file <- function(path) {

app <- "
  library(shiny)
  library(shinyBS2)
  library(mmgeCatalogs)

  mmgeCatalogs::shine(color = \"blue\")
"

fn <- file.path(path, "app.R")

writeLines(app, con = fn)

}