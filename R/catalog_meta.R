# Builds a data.frame of meta data about the catalog that is used when generating
# the shiny application
catalog_meta <- function(data, config) {

  meta <- as.data.frame(do.call(rbind, lapply(seq(ncol(data)), function(i) {
    c(

      name = colnames(data)[i],
      index = i,
      type = class(data[[i]]),
      visible = colnames(data)[i] %in% config$display_options$default_columns,
      filterable = !(colnames(data)[i] %in% config$display_options$no_filter),
      hidden = colnames(data)[i] %in% config$display_options$hidden_columns,
      unique = length(unique(data[[i]]))
    )
  })), stringsAsFactors = FALSE)

  meta$visible <- as.logical(meta$visible)
  meta$index <- as.integer(meta$index)
  meta$filterable <- as.logical(meta$filterable)
  meta$hidden <- as.logical(meta$hidden)
  meta$unique <- as.integer(meta$unique)

  meta$hidden[meta$name == config$display_options$sample_id] <- TRUE

  meta$input <- sapply(seq(nrow(meta)), function(r) {

    if(meta$type[r] == "character") {
      if(meta$unique[r] <= config$display_options$select_max) {
        return("select")
      } else {
        return("text")
      }
    } else if(meta$type[r] %in% c("numeric", "integer")) {
      if(meta$unique[r] <= config$display_options$select_max / 2) {
        return("select")
      } else {
        return("slider")
      }
    } else if(meta$type[r] == "Date") {
      return("date")
    } else if(meta$type[r] == "logical") {
      return("checkbox")
    } else {
      return("text")
    }

  })

  return(meta)

}
