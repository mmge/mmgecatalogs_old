#Function that builds a single column control object that appears in the sidebar
#of the shiny application
columnControl <- function(catalogColumn, id, type = "text", visible = TRUE, filterable = TRUE, dismissable = TRUE, ..., column_data) {

  if(missing(id)) {
    id <- tolower(gsub("[[:punct:]]|[[:space:]]", "_", catalogColumn))
  }

  set_option <- function(filter_options, name, opt_type, value, test_type = type) {
    if(test_type == opt_type & !(name %in% filter_options)) {
      filter_options[[name]] <- value
    }
    return(filter_options)
  }

  filter_options <- list() %>%
    set_option("choices", "select", sort(unique(column_data[!is.na(column_data)]))) %>%
    set_option("multiple", "select", TRUE) %>%
    set_option("min", "slider", min(column_data, na.rm = TRUE)) %>%
    set_option("max", "slider", max(column_data, na.rm = TRUE)) %>%
    set_option("value", "slider", c(min(column_data, na.rm = TRUE), max(column_data, na.rm = TRUE))) %>%
    set_option("start", "date", min(column_data, na.rm = TRUE)) %>%
    set_option("end", "date", min(column_data, na.rm = TRUE)) %>%
    set_option("min", "date", min(column_data, na.rm = TRUE)) %>%
    set_option("max", "date", max(column_data, na.rm = TRUE))


  filter <- switch(type,
                   slider = sliderInput(
                     inputId = sprintf("%s_filter", id),
                     label = NULL,
                     width = "100%",
                     min = filter_options$min,
                     max = filter_options$max,
                     value = filter_options$value,
                     ...
                   ),
                   select = selectInput(
                     inputId = sprintf("%s_filter", id),
                     label = NULL,
                     width = "100%",
                     selected = c(),
                     choices = filter_options$choices,
                     multiple = filter_options$multiple
                   ),
                   text = textInput(
                     inputId = sprintf("%s_filter", id),
                     label = NULL,
                     width = "100%",
                     ...
                   ),
                   date = dateRangeInput(
                     inputId = sprintf("%s_filter", id),
                     label = NULL,
                     width = "100%",
                     start = filter_options$start,
                     end = filter_options$end,
                     min = filter_options$min,
                     max = filter_options$max,
                     ...
                   )
  )

  tags$div(class = "catalog-control", id = id, `data-filter_type` = type, `data-column` = catalogColumn,
           tags$div(class = if(filterable) {"column-display"} else {"column-display full"},
                    tags$label(`for` = sprintf("%s_show", id), title = sprintf("Click to show/hide column for %s", catalogColumn),
                               tags$input(type = "checkbox", id = sprintf("%s_show", id), checked = if(visible) {"checked"}),
                               tags$span(class = "checkbox-label", catalogColumn, title = catalogColumn)
                    ),
#                    if(dismissable) {tags$a(class = "catalog-button dismiss-column", href = "javascript:;", icon("times", class = "fa-fw"), title = sprintf("Click to dismiss filter and column for %s", catalogColumn))},
                    if(filterable) {
                      tagList(
                        tags$a(class = "catalog-button filter-reset hidden", href = "javascript:;", icon("undo", class = "fa-fw"), title = sprintf("Click to reset filter for %s", catalogColumn)),
                        tags$a(class = "catalog-button filter-toggle", `data-toggle` = "collapse", href = sprintf("#%s_collapse", id), icon("filter", class = "fa-fw"), title = sprintf("Click to view filter for %s", catalogColumn))
                      )
                    }
           ),
           if(filterable) {
             tags$div(class = "collapse column-filter", id = sprintf("%s_collapse", id),
                      filter,
                      checkboxInput(sprintf("%s_missing", id), label = "Remove Missing Values", width = "100%")
             )
           }
  )

}
